package com.example.olskr.materialdesignhm.Utils;

import android.content.SharedPreferences;

import com.example.olskr.materialdesignhm.App;

import static android.content.Context.MODE_PRIVATE;

public class Prefs {
    private static final String MAIN_SHARED_PREFERENCES_TAG = "SP";
    private static final String THEME_DEFAULT_ENABLED_KEY = "Default theme";
    private static final String THEME_BLACK_ENABLED_KEY = "Black theme";
    private static final String THEME_PURPLE_ENABLED_KEY = "Purple theme";
    private static volatile Prefs instance = null;
    private SharedPreferences preferences;

    private Prefs() {
        preferences = App.getInstance().getSharedPreferences(MAIN_SHARED_PREFERENCES_TAG, MODE_PRIVATE);
    }

    public static Prefs getInstance() {
        Prefs localInstance = instance;
        if (localInstance == null) {
            //синхронизация по классу
            synchronized (Prefs.class) { //в один момент времени только один объект класса может выполнить этот метод
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Prefs();
                }
            }
        }
        return localInstance;
    }

    public boolean isDefaultThemeEnable() {
        return preferences.getBoolean(THEME_DEFAULT_ENABLED_KEY, false);
    }

    public boolean isBlackThemeEnable() {
        return preferences.getBoolean(THEME_BLACK_ENABLED_KEY, false);
    }

    public boolean isPurpleThemeEnable() {
        return preferences.getBoolean(THEME_PURPLE_ENABLED_KEY, false);
    }

    public void setDefaultThemeStatus(boolean isEnable) {
        preferences.edit().putBoolean(THEME_DEFAULT_ENABLED_KEY, isEnable).apply();
    }

    public void setBlackThemeStatus(boolean isEnable) {
        preferences.edit().putBoolean(THEME_BLACK_ENABLED_KEY, isEnable).apply();
    }

    public void setPurpleThemeStatus(boolean isEnable) {
        preferences.edit().putBoolean(THEME_PURPLE_ENABLED_KEY, isEnable).apply();
    }
}
