package com.example.olskr.materialdesignhm.AllFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.olskr.materialdesignhm.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AboutFragment extends MainFragment {

    @BindView(R.id.content)
    TextView content;

    private final static String TEXT_CONTENT = "В 2014 году на конференции был представлен новый подход к дизайну приложений. " +
            "Это попытка сделать единообразный интерфейс для всех приложений Google, " +
            "неважно где они работают на телефоне, планшете или компьютере. " +
            "А также для всех Андроид приложений. Данный стиль основан на размещении плоской бумаги на экране. " +
            "Бумага тонкая, плоская, но расположенная в трехмерном пространстве, с тенями, с движением. " +
            "Такую бумагу называют квантумной, или цифровой. Если происходит анимация, " +
            "то она и показывает пользователю, что происходит. Однако чрезмерная анимация не нужна, " +
            "никому не интересно ждать пару секунд, пока окно с сообщением налетается по экрану.\n";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.about_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void initLayout(View view, Bundle savedInstanceState) {
        content.setText(TEXT_CONTENT);
    }
}
