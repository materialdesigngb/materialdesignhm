package com.example.olskr.materialdesignhm.AllFragments;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.olskr.materialdesignhm.R;
import com.example.olskr.materialdesignhm.Utils.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity {

    private static final String MAIN_FRAGMENT_TAG = "43ddDcdd-c9e0-4794-B7e6-cf05af49fbf0";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        applyTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        ButterKnife.bind(this);
        initUi(savedInstanceState);
    }

    private void initUi(Bundle savedInstanceState) {

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        initListeners();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, new MainFragment(), MAIN_FRAGMENT_TAG)
                    .commit();
        }
    }

    private void initListeners() {
        navigationView.setNavigationItemSelectedListener(getOnNavigationItemSelectedListener());
    }

    // метод для добавления фрагмента
    private void addFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack("")
                .commit();
    }

    private void applyTheme() {
        setTheme(getThemeFromPreference());
    }

    public int getThemeFromPreference() {
        boolean blackThemeEnabled = Prefs.getInstance().isBlackThemeEnable();
        boolean purpleThemeEnabled = Prefs.getInstance().isPurpleThemeEnable();
        if (blackThemeEnabled) {
            return R.style.MyTheme_Black;
        } else if (purpleThemeEnabled) {
            return R.style.MyTheme_Purple;
        } else {
            return R.style.MyTheme;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    // Меню Action bar - выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                addFragment(new SettingsFragment());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //меню
    private NavigationView.OnNavigationItemSelectedListener getOnNavigationItemSelectedListener() {
        return menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.nav_settings:                  // наши кнопки в меню навигации
                    addFragment(new SettingsFragment());
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                case R.id.nav_info:
                    addFragment(new AboutFragment());
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                case R.id.nav_evaluate:
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                default:
                    break;
            }

            drawer.closeDrawer(GravityCompat.START);
            return true;
        };
    }
}
