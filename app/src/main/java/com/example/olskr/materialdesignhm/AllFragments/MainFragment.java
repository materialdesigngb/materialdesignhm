package com.example.olskr.materialdesignhm.AllFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.olskr.materialdesignhm.App;
import com.example.olskr.materialdesignhm.R;
import com.example.olskr.materialdesignhm.RecycleView.DataSourceBuilder;
import com.example.olskr.materialdesignhm.RecycleView.Soc;
import com.example.olskr.materialdesignhm.RecycleView.SocnetAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainFragment extends BaseFragment {
    @Nullable
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @Nullable
    @BindView(R.id.fab_add_new_photo)
    FloatingActionButton fab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void initLayout(View view, Bundle savedInstanceState) {
        initListener();
        initRecycle();
    }

    private void initListener() {
        fab.setOnClickListener(v -> {
        });
    }

    private void initRecycle() {
        // установим аниматор по умолчанию
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // эта установка служит для повышения производительности системы.
        recyclerView.setHasFixedSize(true);

        // будем работать со встроенным менеджером
        GridLayoutManager gridLayoutManager = new GridLayoutManager(App.getInstance(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        // строим источник данных
        DataSourceBuilder builder = new DataSourceBuilder(getResources());
        final List<Soc> dataSource = builder.build();
        // установим адаптер
        final SocnetAdapter adapter = new SocnetAdapter(dataSource);
        recyclerView.setAdapter(adapter);

        // установить слушателя
        adapter.SetOnItemClickListener((view1, position) ->
                Snackbar.make(view1, String.format("Позиция - %d", position + 1), Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show());
    }
}
