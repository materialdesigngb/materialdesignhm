package com.example.olskr.materialdesignhm.AllFragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.olskr.materialdesignhm.R;
import com.example.olskr.materialdesignhm.Utils.Prefs;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback {

    @BindView(R.id.button_settings_save)
    Button button_settings_save;
    @BindView(R.id.switch_theme)
    RadioGroup radioGroup;
    private boolean defaultThemeStatus = false;
    private boolean blackThemeStatus = false;
    private boolean purpleThemeStatus = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.setting_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    protected void initLayout(View view, Bundle savedInstanceState) {
        getTheme();
        initListener();
    }

    private void initListener() {
        button_settings_save.setOnClickListener(view1 -> {
            Prefs.getInstance().setDefaultThemeStatus(defaultThemeStatus);
            Prefs.getInstance().setBlackThemeStatus(blackThemeStatus);
            Prefs.getInstance().setPurpleThemeStatus(purpleThemeStatus);
            switchTheme();
        });
    }

    private void getTheme() {
        radioGroup.setOnCheckedChangeListener((radioGroup1, checkedId) -> {
            switch (checkedId) {
                case R.id.default_theme_status:
                    defaultThemeStatus = true;
                    blackThemeStatus = false;
                    purpleThemeStatus = false;
                    break;
                case R.id.black_theme_status:
                    defaultThemeStatus = false;
                    blackThemeStatus = true;
                    purpleThemeStatus = false;
                    break;
                case R.id.purple_theme_status:
                    defaultThemeStatus = false;
                    blackThemeStatus = false;
                    purpleThemeStatus = true;
                    break;
            }
        });
    }
    private void switchTheme() {
        new Handler().post(() -> {
            Intent intent = Objects.requireNonNull(getActivity()).getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            getActivity().overridePendingTransition(0, 0);
            getActivity().finish();

            getActivity().overridePendingTransition(0, 0);
            startActivity(intent);
        });
    }
}
